#!/bin/sh

DIR_PREPROD_SBX=/Users/julian.perez/PayU\ gitops/gitops-eks-qa-environments
DIR_PROD=/Users/julian.perez/PayU\ gitops/gitops-eks-prod-envs

# Select environment
read -p "Environment: " ENV
read -p "New version: " NEW_VER

# Go to the repo
if [ "$ENV" = preprod -o "$ENV" = sandbox ]; then
  BASE_DIR="$DIR_PREPROD_SBX"
elif [ "$ENV" = production ]; then
  BASE_DIR="$DIR_PROD"
fi

# Git pull and creating branch (it must be connected to vpn)
cd "$BASE_DIR"
git pull
git checkout -b deploy-"$ENV"-"$NEW_VER"
cd

# Reading images
read -p "Backoffice image: " BACKOFFICE
if [ "$ENV" = preprod ]; then
  read -p "Admin and Secure image: " ADMIN_PREPROD
  SECURE_PREPROD=ADMIN_PREPROD
fi
read -p "Transactional image: " TRANSACTIONAL
read -p "PPNS image: " PPNS
read -p "Gateway image: " GATEWAY

# Change service tags
PATHS=(
  services/$ENV/app/pps/ppn-ar-amex/ppn-ar-amex.yaml
  services/$ENV/app/pps/ppn-ar-cobro-express/ppn-ar-cobro-express.yaml
  services/$ENV/app/pps/ppn-ar-visa/ppn-ar-visa.yaml
  services/$ENV/app/pps/ppn-br-bankslip-itau/ppn-br-bankslip-itau.yaml
  services/$ENV/app/pps/ppn-br-boleto-itau-v2/ppn-br-boleto-itau-v2.yaml
  services/$ENV/app/pps/ppn-br-itau-shopline/ppn-br-itau-shopline.yaml
  services/$ENV/app/pps/ppn-cl-multicaja/ppn-cl-multicaja.yaml
  services/$ENV/app/pps/ppn-cl-transbank-debit/ppn-cl-transbank-debit.yaml
  services/$ENV/app/pps/ppn-co-credibanco/ppn-co-credibanco.yaml
  services/$ENV/app/pps/ppn-mx-bancomer/ppn-mx-bancomer.yaml
  services/$ENV/app/pps/ppn-mx-diestel/ppn-mx-diestel.yaml
  services/$ENV/app/pps/ppn-pe-pago-efectivo/ppn-pe-pago-efectivo.yaml
  services/$ENV/app/pps/ppn-pu/ppn-pu.yaml
  services/$ENV/app/pps/ppp-core-utils-pu/ppp-core-utils-pu.yaml
  services/$ENV/web/backoffice/ppp-web-admin-api/ppp-web-admin-api.yaml
  services/$ENV/web/backoffice/ppp-web-admin/ppp-web-admin.yaml
  services/$ENV/web/backoffice/ppp-web-dispute-api/ppp-web-dispute-api.yaml
  services/$ENV/web/backoffice/ppp-web-gateway/ppp-web-gateway.yaml
  services/$ENV/web/backoffice/ppp-web-merchant-api/ppp-web-merchant-api.yaml
  services/$ENV/web/backoffice/ppp-web-secure/ppp-web-secure.yaml
  services/$ENV/web/backoffice/ppp-web-transfers-api/ppp-web-transfers-api.yaml
  services/$ENV/web/transactional/ppp-web-banking-api/ppp-web-banking-api.yaml
  services/$ENV/web/transactional/ppp-web-bridge/ppp-web-bridge.yaml
  services/$ENV/web/transactional/ppp-web-crons/ppp-web-crons.yaml
  services/$ENV/web/transactional/ppp-web-payments-api/ppp-web-payments-api.yaml
  services/$ENV/web/transactional/ppp-web-reports-api/ppp-web-reports-api.yaml
)

rootString="image: regestum-cloud.payulatam.com:443"

br_boleto_itau=0 #flag to enable particular logic due to different structure

for element in "${PATHS[@]}"
do
  cd "$BASE_DIR"

  # get the file name, path and line string to be replaced
  oldIFS=$IFS
  IFS='/'
  read -a split <<< "$element"
  IFS=$oldIFS

  FILE_NAME="${split[5]}"

  path=("${split[0]}" "${split[1]}" "${split[2]}" "${split[3]}" "${split[4]}")
  SERVICE_PATH=$(printf "%s/" "${path[@]}")

  #Updating service tag
  cd "$SERVICE_PATH"

  if [[ "$FILE_NAME" != ppn-br-boleto-itau-v2.yaml || "$ENV" != preprod ]]; then
    string=("$rootString" "${split[3]}" "${split[4]}")
    STRING=$(printf "%s/" "${string[@]}")
    STRING=$(echo ${STRING:0:$((${#STRING}-1))}:)

    N_LINE=$(grep -n -m 1 "$STRING" $FILE_NAME | cut -f1 -d ":")
    OLD_LINE=$(sed -n "$N_LINE"p $FILE_NAME)

    oldIFS=$IFS
    IFS=':'
    read -a array <<< "$OLD_LINE"
    IFS=$oldIFS
    array1=("${array[1]}" "${array[2]}" "${array[3]}")
    TO_REPLACE=$(printf "%s:" "${array1[@]}")
    TO_REPLACE=$(echo "${TO_REPLACE%?}")
    echo old line: $TO_REPLACE
    OLD_IMAGE="${array[3]}"
  fi

  if [ "${split[3]}" = pps ]; then

    # logic to deal with the particular structure of ppn-br-boleto-itau-v2.yaml in preprod
    if [[ "$ENV" == preprod && "${split[4]}" == ppn-br-boleto-itau-v2 ]]; then
      br_boleto_itau=1
      STRING="image: pps/ppn-br-boleto-itau-"

      N_LINE=$(grep -n -m 1 "$STRING" $FILE_NAME | cut -f1 -d ":")
      OLD_LINE=$(sed -n "$N_LINE"p $FILE_NAME)

      oldIFS=$IFS
      IFS=':'
      read -a line_preprod <<< "$OLD_LINE"
      IFS=$oldIFS
      TO_REPLACE="${line_preprod[1]}"
      echo old line: $TO_REPLACE

      oldIFS=$IFS
      IFS='-'
      read -a line_preprod1 <<< "$TO_REPLACE"
      IFS=$oldIFS

      y=0
      line_preprod2=()
      line_preprod3=()
      for x in "${line_preprod1[@]}"
      do
        if [ "$y" -lt 3 ]; then
          line_preprod2+=("$x")
        fi
        if [ "$y" -gt 3 ]; then
          line_preprod3+=("$x")
        fi
        y=$(( $y + 1 ))
      done

      OLD_IMAGE=$(printf "%s-" "${line_preprod3[@]}")
      OLD_IMAGE=$(echo "${OLD_IMAGE%?}")

      NEW_IMAGE="$PPNS"

      REPLACE=$(printf "%s-" "${line_preprod2[@]}")

      REPLACE="$REPLACE"$NEW_IMAGE
      echo new line: "$REPLACE"

      sed -i '' "s/$OLD_IMAGE/$NEW_IMAGE/g" $FILE_NAME # in linux remove '' after -i arg
    fi

    array1[2]="$PPNS"
  elif [ "${split[3]}" = backoffice ]; then

    if [ "${split[4]}" = ppp-web-gateway ]; then
      array1[2]="$GATEWAY"
    else
      array1[2]="$BACKOFFICE"
    fi

    #special logic for admin and secure in preprod
    if [[ "${split[4]}" == ppp-web-admin || "${split[4]}" == ppp-web-secure ]] && [ "$ENV" = preprod ]; then
          array1[2]="$ADMIN_PREPROD"
    fi

  elif [ "${split[3]}" = transactional ]; then
      array1[2]="$TRANSACTIONAL"
  fi

  if [ "$br_boleto_itau" = 0 ]; then
    REPLACE=$(printf "%s:" "${array1[@]}")
    REPLACE=$(echo "${REPLACE%?}")
    echo new line: $REPLACE
    NEW_IMAGE="${array1[2]}"

    sed -i '' "s/$OLD_IMAGE/$NEW_IMAGE/g" $FILE_NAME # in linux remove '' after -i arg
  else
    br_boleto_itau=0
  fi

  cd

done

# publish changes to remote repo
cd "$BASE_DIR"
git add -A
COMMIT_MSG="updating service tags deploy-""$ENV"-"$NEW_VER"
git commit -m "$COMMIT_MSG"
git push --set-upstream origin deploy-"$ENV"-"$NEW_VER"

# delete local and remote pushed branch
# git checkout master
# git branch -D <branch>
# git clean -f
# git push origin -d <branch>
